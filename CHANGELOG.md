## 0.3.0 (2020-06-11)

Renamed qqlistreview function to \_\_qqlistreview.
Added an option to open the next task in the list result from a qqlist call.

## 0.2.0 (2020-06-10)

Choose the \_project tag while adding a new task using the qqadd() function.

## 0.1.1 (2020-06-05)

Code cleanup. No functional changes.

## 0.1.0 (2020-06-04)

First version.

Still on early stages, but being used as a daily driver.
