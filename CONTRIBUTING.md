# Contributing

You can contribute with:

- Open a PR with your improvements and suggestions
- Open an issue with a bug report, a suggestion or a feature request
- Spread the word

# Contact

https://paapereira.xyz
