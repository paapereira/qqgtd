# qqgtd: project QQ for a text based Getting Things Done approach

A simple shell script with functions to interact with a text based Getting Things Done (GTD) approach.

It's basically a helper to add new tasks and to search tasks.
Moving, renaming and editing tasks is not in the scope of this project.
I use zim wiki and neovim in conjunction with qqgtd.

This is a very early project meant for my needs that I'm sharing.
Suggestions and questions are welcome.

### CHANGELOG

Please see the [CHANGELOG](CHANGELOG.md) for a release history.

## TODO

- [x] Write first working version
- [x] Use it as a daily driver
- [ ] Finish the first draft of the README file
  - [ ] document all the functions
  - [ ] explain the journaling part
- [ ] Review code
- [ ] Organize and finish some ideias not yet implemented
  - [ ] play with the idea of an '\_extended' property
  - [ ] allow for the \*.md extention

## Table of Contents

- [References](#references)
- [Installation](#installation)
- [Configure .qqgtdrc](#configure-qqgtdrc)
- [Concepts](#concepts)
  - [The Notebook](#the-notebook)
  - [Tasks Naming Convention and Structure](#tasks-naming-convention-and-structure)
  - [My Tasks Structure](#my-tasks-structure)
- [Functions](#functions)
  - [qqsetcontext()](#qqsetcontext)

## References

- [Getting Things Done book](https://en.wikipedia.org/wiki/Getting_Things_Done)
- [QQ inspiration](https://wcm1.web.rice.edu/more-plain-text-gtd.html)
- [zim wiki](https://zim-wiki.org/)
- [neovim](https://neovim.io/)

## Installation

This project is intended to be used in Linux and Windows (probably will work in macOS).

My setup in Linux:

- Arch Linux
- zsh shell

My setup in my work Windows machine:

- I'm using [alacritty](https://github.com/alacritty/alacritty)
- bash shell

0. Install dependencies

- [fzf](https://github.com/junegunn/fzf)
- [ripgrep](https://github.com/BurntSushi/ripgrep)

1. Clone the project

```bash
git clone https://gitlab.com/paapereira/qqgtd
```

2. Copy the .qqgtdrc file to your home directory

```bash
cp qqgtd/.qqgtdrc ~/.qqgtdrc
```

3. Copy the 'templates/new_task.txt' to your 'templates' folder for each of your 'contexts'.

4. Source the .qqgtdrc file in your bashrc or zshrc

```bash
source ~/.qqgtdrc
```

5. Read the rest of the README and configure your .qqgtdrc and new_task.txt template.

## Configure .qqgtdrc

Configure the following variables to match your setup.

- qqgtd_source: the directory where you've cloned the project
- qqgtd_notebook: location of your notebook
- qqgtd_defaulteditor: editor to open your notes from the terminal
- qqgtdcontexts: add lines for your different contexts, like 'work' and 'personal'

```bash
#===============================================================================

# --- USER CONFIGURATION -------------------------------------------------------

# qqgtd source directory

qqgtd_source=~/qqgtd

# notebook base directory

qqgtd_notebook=~/notebook

# editors

qqgtd_defaulteditor=nvim

# contexts

qqgtdcontexts="\
  mode=work;      context=${qqgtd_notebook}/work;\
  mode=personal;  context=${qqgtd_notebook}/personal;\
  "

#===============================================================================

```

## Concepts

The main concept it's the use of text files to manage you GTD trusted system.

Besides that there are only 2 concepts: the Notebook and the Tasks naming convention and structure.

The rest is for you to decide how is the best way to manage your system.

### The Notebook

A notebook is basically the base directory of your system.
It has a directory for each 'context' you want. A context can be 'work' and 'personal' for example.
Each context has at least the following directories: 'tasks', 'journal' and 'templates'. A 'notes' folder is also recommended.

```text
notebook
├── personal
│   ├── journal
│   ├── notes
│   ├── tasks
│   └── templates
├── work
│   ├── journal
│   ├── notes
│   ├── tasks
│   └── templates
└── others...
```

### Tasks Naming Convention and Structure

In 'qqgtd' a task is a .txt file, anywhere in the 'tasks' directory, with no spaces in the name and starting with 'qq*' or 'qqq*'.
If the .txt file doesn't start with 'qq*' or 'qqq*' then it's a completed task.
A 'qq*' task is a regular task. A 'qqq*' task is a more important or started task.
The creation date is also in task name. You can change this date for the completion date or remove it if you want. By default, new taks will have it.

Examples of tasks names:

```text
2020-05-29_My_completed_task.txt
qq_2020-05-29_My_regular_task.txt
qqq_2020-05-29_My_important_task.txt
```

For the 'qqgtd' search functions to work you must use a task template with some known minimum structure.
You can use and adapt the 'templates/new_task.txt' template and use it in each of your 'templates' directory.

You can use the 'qqadd' function to add tasks. This function will replace the following variables in the template:

- <TASK_NAME> (e.g. qq 2020-06-01 My new task)
- <CREATION_DATE_1> (e.g. 2020-06-01T10:08:55+01:00)
- <CREATION_DATE_2> (e.g. 2020-06-01 10:08:55)
- <LOG_DATE> (e.g. 2020-06-01 10:08:55, Monday)

Besides these variables you can use, you **must** have the following lines:

- \_created_at: date and time of your task. You can change it. Currently it's used for your information. (e.g. `_created_at: "2020-06-01 10:08:55"`)
- \_updated_at: date and time of your latest change of the task. You can change it every time you update your task is you want. (e.g. `_updated_at: "2020-06-01 10:08:55"`)
- \_completed_at: date and time of your task completion for your information. You can add the date and time when you've completed a task. (e.g. `_completed_at: "2020-06-01 10:08:55"`)
- \_project: the name of the project of the task. (e.g. `_project: @my_project`)
- \_context: list of contexts or tags for the task. ( e.g. `_context: @work @jon_doe @daily`)
- \_due: due date for the task. (e.g. `_due: @20200601`)
- \_repeat: for tasks with repetion. Useful if you don't and a '\_due' date set. Can be a list and the 2 formats: @yyyymm01 for the first day of every day for example or @monday. (e.g. `_repeat: @yyyymm01`)

```text
_created_at:
_updated_at:
_completed_at:
_project:
_context:
_due:
_repeat:
```

Here is my template (aligned with zim wiki):

```text
Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: <CREATION_DATE_1>

====== <TASK_NAME> ======

--------------------

_created_at: "<CREATION_DATE_2>"
_updated_at: "<CREATION_DATE_2>"
_completed_at:
_project: @project
_context: @context1 @context2
_due:
_repeat:
_extended:

--------------------

===== Next Steps =====



===== References =====



===== Log =====

=== <LOG_DATE> ===
```

### My Tasks Structure

You tasks directory can be organized as you wish.

An 'inbox' directory is always created for new tasks.

I like to have a 'projects' directory to keep all my projects, a 'completed' directory to move older completed tasks and a 'misc' directory for single tasks.

```text
notebook/sibs/tasks/
├── completed
├── inbox
├── misc
├── projects
│   ├── project1
│   ├── project2
│   └── ...
└── ...
```

## Functions

### qqsetcontext()
